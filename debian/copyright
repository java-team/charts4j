Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: charts4j
Source: https://github.com/julienchastang/charts4j
Comment: We remove the provided javadoc, the jar that is provided in lib/, and
         the files that are provided for Eclipse or ant, as we chose to build
         with Maven.
Files-Excluded: */doc
                */lib
                */.settings
                */build.properties
                */build.xml
                */.classpath
                */.project

Files: *
Copyright: 2011 Julien Chastang
License: Expat

Files: src/main/java/com/googlecode/charts4j/collect/Preconditions.java
Copyright: 2007, Google Inc.
License: Apache-2.0

Files: debian/*
Copyright: 2020-2021 Pierre Gruet <pgt@debian.org>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 IN THE SOFTWARE.

License: Apache-2.0
 On Debian systems, you can read the full text of the Apache
 License in ‘/usr/share/common-licenses/Apache-2.0’.
